

import  { SweetAlert } from 'node_modules/sweetalert/typings/core';

declare module 'sweetalert' {
    var sweetalert: SweetAlert ;
    export = sweetalert;
}

