@extends('Admin.master')



@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">انتساب نقش</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('roles.index')}}" class="btn btn-secondary">لیست نقش ها</a>
                    <a href="{{route('users.all')}}" class="btn btn-secondary">لیست کاربران</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">

        @include('Admin.section.errors')

        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">انتساب نقش</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->

            <form action="{{route('users.roles',$user->id)}}" method="post" role="form">
                @csrf
                   <div class="m-3">
                       <div class="form-group">
                           <label for="title">نقش ها</label>

                           <select class="form-control selectpicker" name="roles[]" multiple>
                               @foreach ($roles as $role)
                                   <option value="{{$role->id}}" {{$role->users->contains('id',$user->id) ? 'selected' : ''}}>{{$role->label}}</option>
                               @endforeach

                           </select>
                       </div>
                   </div>

                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">ارسال</button>
                </div>
            </form>
        </div>
    </div>

@endsection
