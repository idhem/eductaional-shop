@extends('Admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">ویدئو ها</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('payments.index')}}?status=1" class="btn btn-primary">پرداخت شده ها</a>
                    <a href="{{route('payments.index')}}?status=0" class="btn btn-primary">پرداخت نشده ها</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">
        @include('Admin.section.flash')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">پرداختی ها</h3>
            </div>
            <!-- /.card-header -->

            @if($payments->count() > 0)
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>موضوع</th>
                            <th>تاریخ ارسال</th>
                            <th>مبلغ پرداختی</th>
                            <th>وضعیت</th>
                            <th>کاربر</th>
                            <th>شماره تراکنش</th>
                        </tr>
                        @foreach($payments as $key => $payment)
                            <tr>
                                <td>{{$key + 1}}.</td>
                                <td>{{ $payment->course->title  }}</td>
                                <td>
                                    {{ jdate($payment->created_at)->format('%A, %d %B %y')  }}
                                </td>
                                <td>
                                    {{ $payment->course->price  }}
                                </td>
                                <td>
                                    @if($payment->status === 0)
                                    <b class="bg-danger  p-2 rounded">پرداخت نشده</b>
                                    @else
                                        <b class="bg-success p-2 rounded">پرداخت شده</b>
                                    @endif
                                </td>
                                <td>{{ $payment->user->name  }}</td>
                                <td>{{ $payment->trans_number  }}</td>

                            </tr>
                        @endforeach
                        </tbody></table>
                </div>

                {{$payments->render()}}
            @else
                <div class="callout callout-warning m-4" role="alert">
                    موردی یافت نشد
                </div>
        @endif

        <!-- /.card-body -->
        </div>
    </div>

@endsection
