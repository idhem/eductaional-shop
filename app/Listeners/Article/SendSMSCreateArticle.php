<?php

namespace App\Listeners\Article;

use App\Events\ArticleNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSMSCreateArticle
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ArticleNotification  $event
     * @return void
     */
    public function handle(ArticleNotification $event)
    {

    }
}
