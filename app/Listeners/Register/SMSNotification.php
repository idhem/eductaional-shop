<?php

namespace App\Listeners\Register;

use App\Events\RegisterNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SMSNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterNotification  $event
     * @return void
     */
    public function handle(RegisterNotification $event)
    {
         $article = $event->article;
         dd($article);
    }
}
