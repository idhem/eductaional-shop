@extends('Admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">کامنت های تایید شده</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('comments.approvedIndex')}}" class="btn btn-primary">کامنت های تایید نشده</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">
        @include('Admin.section.flash')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">جدول ۴</h3>
            </div>
            <!-- /.card-header -->

            @if($comments->count() > 0)
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>کاربر</th>
                            <th>متن</th>
                            <th>تاریخ ارسال</th>
                            <th>تنظیمات</th>
                        </tr>
                        @foreach($comments as $key => $comment)
                            <tr>
                                <td>{{$key + 1}}.</td>
                                <td>{{ $comment->user->name }}</td>
                                <td>{{ $comment->body  }}</td>
                                <td>
                                    {{ jdate($comment->created_at)->format('%A, %d %B %y')  }}
                                </td>
                                <td>
                                    <form action="{{route('comments.destroy',$comment->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <button type="submit" class="btn btn-sm btn-danger btn-flat"><span class="material-icons">delete</span></button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody></table>
                </div>
            @else
                <div class="callout callout-warning m-4" role="alert">
                    موردی یافت نشد
                </div>
        @endif

        <!-- /.card-body -->
        </div>
    </div>

@endsection
