@if($errors->any())

    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> توجه!</h5>
        <div class="d-flex flex-column justify-content-around">
            @foreach($errors->all() as $key => $error)

                <div>{{$key + 1}}. {{$error}}</div>
            @endforeach
        </div>

    </div>
@endif
