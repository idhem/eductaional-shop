<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;

use App\Course;
use App\Article;
use App\Events\ArticleEvent;
use SEO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use vendor\project\StatusTest;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //  $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        SEO::setTitle('وبسایت آموزشی');

        $articles = Cache::remember('articles', Carbon::now()->addMinute(10), function () {
            return Article::latest()->take(6)->get();
        });
        $courses = Cache::remember('courses', Carbon::now()->addMinute(10), function () {
            return Course::latest()->take(4)->get();
        });

        return view('Web\index', compact('articles', 'courses'));
    }


    public function contact()
    {
        return view('Web.contact-us');
    }

    public function about()
    {
        return view('Web.about-us');
    }

    public function categories()
    {
        $category =  \App\Category::whereLabel(Request('name'))->first() ?? abort(404,'متاسفانه این صفحه یافت نشد');
        $courses = $category->courses()->latest()->take(12)->get();
        $articles = $category->articles()->latest()->take(6)->get();

        return view('Web.categories',compact('courses','articles','category'));
    }

    public function storeComment(Request $request)
    {
        $validData = Validator::make($request->all(), [
            'body' => 'required',
            'commentable_id' => 'required',
            'commentable_type' => 'required'
        ]);

        if ($validData->fails()) return response([
            'status' => 'bad request',
            'data' => $validData->errors()
        ], 400);

        Comment::create(array_merge(
            $request->all(),
            [
                'user_id' => auth()->user()->id,
                'parent_id' => $request->has('parent_id') ?? $request->parent_id
            ]

        ));

        return response(['status' => 'ok'], 200);
    }


    public function searchAjax(Request $request) {
        $validate = Validator::make($request->all(),[
            'text'=> 'required|min:3'
        ]);

        if($validate->fails())
            return response()->json([
                'data' => $validate->errors(),
                'status' => 'Bad Request'
            ]);
        $articles = Article::search($request->text);

        return response([
            'data' => [
              'articles' => $articles
            ],
            'status' => 'OK',
        ],200);

    }

}
