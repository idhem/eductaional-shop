<?php

namespace App\Http\Middleware;


use Closure;

class RedirectIfLevelNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user  = $request->route()->parameter('user');

        return $user->level == 'admin' ? $next($request) : abort('403');
    }
}
