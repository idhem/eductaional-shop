<?php

function user()
{
    return auth()->user();
}

function DaysFromNow($time) {
    
    $time = strtotime($time);
    $datediff =  max($time - time(),0);
    return round( $datediff / (60 * 60 * 24));
}