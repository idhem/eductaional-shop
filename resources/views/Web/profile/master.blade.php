@extends('master');


@section('content')
<ul class="nav nav-tabs mt-3">
    <li class="nav-item">
      <a class="nav-link {{ $courrentRouteName == 'user.profile' ? 'active' : ''}} " href="{{ route('user.profile') }}">پروفایل</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ $courrentRouteName == 'user.profile.history' ? 'active' : ''}} " href="{{ route('user.profile.history') }}">تاریخچه پرداخت</a>
    </li>

    <li class="nav-item">
      <a class="nav-link {{ $courrentRouteName == 'user.profile.vip' ? 'active' : ''}} " href="{{ route('user.profile.vip') }}">شارژ vip</a>
    </li>
  </ul>


  {{ $slot }}

@endsection

