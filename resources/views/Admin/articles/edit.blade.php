@extends('Admin.master')

@section('scripts')
    <script src="/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('body',{
            filebrowserBrowseUrl: '/admin/panel/fileUpload?&_token={{csrf_token()}}',
            filebrowserUploadUrl: '/admin/panel/fileUpload?&_token={{csrf_token()}}'
        });
    </script>

@endsection

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">مقالات</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('articles.index')}}" class="btn btn-secondary">لیست مقالات</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">

        @include('Admin.section.errors')

        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">ویرایش مقاله</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->

            <form action="{{route('articles.update',$article->id)}}" method="post" enctype="multipart/form-data" role="form">
                @method('PUT')
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">موضوع</label>
                        <input type="text" name="title" class="form-control" id="title" value="{{$article->title}}" placeholder="موضوع را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="description">توضیحات کوتاه</label>
                        <input type="text" name="description" class="form-control" id="description" value="{{$article->description}}" placeholder="توضیحات را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="title">دسته بندی های مربوطه</label>

                        <select class="form-control selectpicker"  data-live-search="true" name="categories[]" multiple>
                            @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">ارسال عکس</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="images" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">انتخاب عکس مقاله</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Upload</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="body">متن اصلی</label>
                        <textarea  class="form-control" name="body" id="body" rows="5" >{{$article->body}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="tags">تگ ها</label>
                        <input type="text" name="tags" class="form-control" id="tags" value="{{$article->description}}" placeholder="تگ ها را وارد کنید">
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">ویرایش</button>
                </div>
            </form>
        </div>
    </div>

@endsection
