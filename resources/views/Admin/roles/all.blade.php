@extends('Admin.master')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">نقش ها</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('roles.create')}}" class="btn btn-primary">افزودن نقش</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">
        @include('Admin.section.flash')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">لیست نقش ها</h3>
            </div>
            <!-- /.card-header -->

            @if($roles->count() > 0)
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>برچسب</th>
                            <th>تنظیمات</th>
                        </tr>
                        @foreach($roles as $key => $role)
                            <tr>
                                <td>{{$key + 1}}.</td>
                                <td>{{ $role->name  }}</td>
                                <td>
                                    {{$role->label}}
                                </td>
                                <td>
                                    <form action="{{route('roles.destroy',$role->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="{{route('roles.edit',$role->id)}}" class="btn btn-sm btn-info btn-flat">
                                                <span class="material-icons">edit</span>
                                            </a>
                                            <button type="submit" class="btn btn-sm btn-danger btn-flat"><span class="material-icons">delete</span></button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody></table>
                </div>
                {{$roles->render()}}
            @else
                <div class="callout callout-warning m-4" role="alert">
                    موردی یافت نشد
                </div>
        @endif

        <!-- /.card-body -->
        </div>
    </div>


@endsection
