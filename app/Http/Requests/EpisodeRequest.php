<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EpisodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       switch($this->method()) 
       {
           case 'POST':
                return [
                'course_id' => 'required',
                'type' => 'required',
                'title' => 'required',
                'body' => 'required|min:5',
                'videoUrl' => 'required',
                'time' => 'required|regex:/^(\d{2}:)?\d{2}:\d{2}$/',
                'tags' => 'required',
                'number'=> 'required'
                    
                ];
               break;
            case 'PUT':
                return [
                    'course_id' => 'required',
                    'type' => 'required',
                    'title' => 'required',
                    'body' => 'required|min:5',
                    'videoUrl' => 'required',
                    'time' => 'required|regex:/^(\d{2}:)?\d{2}:\d{2}$/',
                    'tags' => 'required',
                    'number'=> 'required'
                ];
                break;
       }
    }
}
