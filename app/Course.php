<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Course
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $title
 * @property string $description
 * @property string $price
 * @property string $body
 * @property array $images
 * @property string $time
 * @property string $slug
 * @property string $tags
 * @property int $viewCount
 * @property int $commentCount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Episode[] $episodes
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereViewCount($value)
 * @mixin \Eloquent
 */
class Course extends Model
{
    protected $guarded = [];
    use Sluggable;

    protected $casts = [
        'images' => 'array'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function setBodyAttribute($value)
    {

        $this->attributes['description'] = substr(preg_replace('/<[^>]*>]/', '', $value), 0, 200);
        $this->attributes['body'] = $value;
    }

    public function episodes()
    {
        return $this->hasMany(\App\Episode::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function path()
    {
        return '/courses/' . $this->slug;
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function categories()
    {
        return $this->morphToMany(Category::class,'categoryable');
    }

}
