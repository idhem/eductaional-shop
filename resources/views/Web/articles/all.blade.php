@extends('master')


@section('content')
    <div class="col-md-12 my-5">
        <div class="row">
            <div class="col-sm-12">
                <h3>مقالات</h3>
            </div>
            @foreach ($articles as $article)
                <div class="col-sm-4 col-lg-4 col-md-4">
                    <div class="thumbnail card">
                        <div style="height: 215px;overflow: hidden">
                            <img class="w-100"  src="{{ $article->images['480'] }}" alt="{{ $article->title }}">
                        </div>
                        <div class="caption mx-2">
                            <h4><a class="font-size-7" href="{{ $article->path() }}">{{ $article->title }}</a>
                            </h4>
                            <p class="font-size-5">  {!! str_limit(strip_tags($article->body),140) !!} </p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right">{{ $article->viewCount }} بازدید</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    {{ $articles->render()  }}
@endsection
