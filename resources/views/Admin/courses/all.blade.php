@extends('Admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">دوره ها</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('courses.create')}}" class="btn btn-primary">افزودن دوره</a>
                    <a href="{{route('episodes.index')}}" class="btn btn-danger">همه ویدئو</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">
        @include('Admin.section.flash')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">دوره</h3>
            </div>
            <!-- /.card-header -->

            @if($courses->count() > 0)
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>موضوع</th>
                            <th>تاریخ ارسال</th>
                            <th>زمان</th>
                            <th>بازدید</th>
                            <th>کامنت</th>
                            <th>تنظیمات</th>
                        </tr>
                        @foreach($courses as $key => $course)
                            <tr>
                                <td>{{$key + 1}}.</td>
                                <td>{{ $course->title  }}</td>
                                <td>
                                    {{ jdate($course->created_at)->format('%A, %d %B %y')  }}
                                </td>
                                <td>{{ $course->time  }}</td>
                                <td>{{ $course->viewCount  }}</td>
                                <td>{{ $course->commentCount  }}</td>
                                <td>
                                    <form action="{{route('courses.destroy',['id' => $course->id])}}" method="post">
                                        @method('delete')
                                        @csrf
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{route('courses.episodes',$course->id)}}" class="btn btn-sm btn-info btn-flat">
                                            <span class="material-icons">switch_video</span>
                                        </a>
                                        <a href="{{route('courses.edit',$course->id)}}" class="btn btn-sm btn-info btn-flat">
                                            <span class="material-icons">edit</span>
                                        </a>
                                        <button type="submit" class="btn btn-sm btn-danger btn-flat"><span class="material-icons">delete</span></button>
                                    </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody></table>
            </div>
            @else
                <div class="callout callout-warning m-4" role="alert">
                  موردی یافت نشد
                </div>
        @endif
            <div class="text-center">
                {{ $courses->render()  }}
            </div>
            <!-- /.card-body -->
        </div>
    </div>

    
@endsection
