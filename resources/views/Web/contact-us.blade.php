@extends('master')


@section('content')

<section class="container my-5">
    <div class="card p-3">
        <div class="w-100 text-center">
            <h3 class="font-size-5 mb-4">ارتباط با ما</h3>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <form action="">
                    <div class="form-group">
                        <label for="">نام </label>
                        <input type="text"
                               class="form-control" name="name" id="" aria-describedby="helpId" placeholder="">
                    </div>

                    <div class="form-group">
                        <label for="">تلفن همراه</label>
                        <input type="text"
                               class="form-control" name="phone" id="" aria-describedby="helpId" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="">تلفن </label>
                        <input type="text"
                               class="form-control" name="telephone" id="" aria-describedby="helpId" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="">پست الکترونیکی </label>
                        <input type="text"
                               class="form-control" name="email" id="" aria-describedby="helpId" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="">اظهارات</label>
                        <textarea class="form-control" name="" id="" rows="3"></textarea>
                    </div>
                    <button type="button" class="btn btn-primary">ارسال</button>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
