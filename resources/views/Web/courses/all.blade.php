@extends('master')


@section('content')
    <div class="col-md-12 my-5">
        <div class="row">
            <div class="col-sm-12">
                @if(isset($courseCategory))
                    <h3> دوره های مربوط به {{$courseCategory->name}} </h3>
                    @else
                    <h3> همه دوره ها</h3>

                @endif

            </div>
            @foreach ($courses as $course)
                <div class="col-md-3  col-sm-6 hero-feature">
                    <div class="thumbnail card">
                        <div style="height: 215px;overflow: hidden">
                            <img class="w-100"  src="{{ $course->images['480'] }}" alt="{{ $course->title }}">
                        </div>
                        <div class="caption mx-2">
                            <h3><a class="font-size-7" href="{{ $course->path() }}">{{ $course->title }}</a></h3>
                            <p class="font-size-5"> {!! str_limit(strip_tags($course->body),120) !!} </p>
                            <p>
                                <a href="{{ $course->path() }}" class="btn btn-primary">خرید</a> <a href="{{ $course->path() }}" class="btn btn-default">اطلاعات بیشتر</a>
                            </p>
                        </div>
                        <div class="ratings">
                            <p class="pull-left">{{ $course->viewCount }} بازدید</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    {{ $courses->render()  }}
@endsection
