<?php

namespace App\Rules;

use GuzzleHttp\Client;
use Illuminate\Contracts\Validation\Rule;

class GoogleRecaptcha implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $client = new Client();
        $response = $client->post('https://www.google.com/recaptcha/api/siteverify',[
            'headers' => [
              ' Content-Type' => 'application/json'
            ],
            'form_params' => [
                'secret' => config('services.google.recaptcha.secret_key'),
                'response' => $value,
                'remoteip' => Request()->ip()
            ],
        ]);

        $response = json_decode($response->getBody());
        return $response->success;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.googleRecaptcha');
    }
}
