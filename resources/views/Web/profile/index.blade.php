@component('Web.profile.master')
<ul class="list-group">
    <li class="list-group-item">نام : {{ user()->name }} </li>
    <li class="list-group-item">ایمیل : {{ user()->email }}</li>
    <li class="list-group-item">زمان باقی مانده اکانت  {{ DaysFromNow(user()->vipTime) }} روز دیگر</li>
  </ul>
@endcomponent