<!-- Footer Section -->
<footer style="position: absolute; bottom: 0;width: 100%;margin-top: 20px">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mt-5">
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                   

                    <li class="nav-item">
                        <a class="nav-link" href="/contact-us">تماس با ما</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/about-us">درباره ما</a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>
</footer>
<!-- / Footer Section -->
