@extends('Admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">ویدئو ها</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('episodes.create')}}" class="btn btn-primary">افزودن ویدئو</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">
        @include('Admin.section.flash')
        <div class="card">
           
            <!-- /.card-header -->

            @if($episodes->count() > 0)
            <div class="card-header">
                <h3 class="card-title">{{ $episodes[0]->course->title }}</h3>
            </div>
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>موضوع</th>
                            <th>تاریخ ارسال</th>
                            <th>زمان</th>
                            <th>بازدید</th>
                            <th>کامنت</th>
                            <th>تنظیمات</th>
                        </tr>
                        @foreach($episodes as $key => $episode)
                            <tr>
                                <td>{{$key + 1}}.</td>
                                <td>{{ $episode->title  }}</td>
                                <td>
                                    {{ jdate($episode->created_at)->format('%A, %d %B %y')  }}
                                </td>
                                <td>{{ $episode->time  }}</td>
                                <td>{{ $episode->viewCount  }}</td>
                                <td>{{ $episode->commentCount  }}</td>
                                <td>
                                    <form action="{{route('episodes.destroy',$episode->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{route('episodes.edit',$episode->id)}}" class="btn btn-sm btn-info btn-flat">
                                            <span class="material-icons">edit</span>
                                        </a>
                                        <button type="submit" class="btn btn-sm btn-danger btn-flat"><span class="material-icons">delete</span></button>
                                    </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody></table>

                        {{ $episodes->render() }}
            </div>
            @else
                <div class="callout callout-warning m-4" role="alert">
                  موردی یافت نشد
                </div>
        @endif

            <!-- /.card-body -->
        </div>
    </div>

@endsection
