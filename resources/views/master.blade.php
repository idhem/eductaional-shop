<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    {!! SEO::generate() !!}
    <!-- Fonts -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    @yield('styles')
    <!-- Styles -->

    @yield('scripts-header')
</head>

<body>

<section style="position: relative">
    @include('section.nav')

    <main id="app" role="main" style="min-height: 78vh" class="container mt-4 mb-5">

        @yield('content')

    </main>
    @include('section.footer');
</section>


<!-- JavaScript Dependencies -->
<script src="/js/app.js"></script>
@include('sweet::alert')
@yield('scripts')
</body>
</html>
