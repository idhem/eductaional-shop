<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Episode;
use App\Http\Requests\EpisodeRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EpisodeController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $episodes = Episode::latest()->paginate();
        return view('Admin.episodes.all',compact('episodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        return view('Admin.episodes.create',
        compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EpisodeRequest $request)
    {
        Episode::create($request->all());
        $this->setCourseTime($request);
        return redirect(route('courses.episodes',$request->course_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function show(Episode $episode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function edit(Episode $episode)
    {
        $courses = Course::latest()->get();
        return view('Admin.episodes.edit',
            compact('courses','episode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function update(EpisodeRequest $request,Episode  $episode)
    {
        $lastTime = $episode->time;
        $episode->update($request->all());
        $this->setCourseTime($request , 3 , $lastTime);
        return redirect(route('courses.episodes',$request->course_id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function destroy(EpisodeRequest $episode)
    {
        $episode::destroy($episode->id);
        $this->setCourseTime($episode , 2);
        return redirect(route('episodes.index'));
    }


}
