<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Rules\GoogleRecaptcha;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Socialite;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {

        $googleUser = Socialite::driver('google')->user();

        $user = User::where('email',$googleUser->email)->first();

        if(!$user) {
            $user = User::create([
                'name' => $googleUser->name,
                'email' => $googleUser->email,
                'password' => bcrypt($googleUser->id . $googleUser->name),
                'isActive' => true
            ]);
        }

        if(!$user->isActive) {
            $user->update([
                'isActive' => true
            ]);
        }

        auth()->loginUsingId($user->id);

        return redirect(route('home'));

    }


    protected function validateLogin(Request $request)
    {

        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
            'g-recaptcha-response' => new GoogleRecaptcha
        ]);
    }

    public function verifyGoogleRecaptcha()
    {
//        $token = request()->token;
//        $action = request('action');


        return response()->json([
            'status' => 200
        ]);
    }


    protected function authenticated(Request $request, $user)
    {
        alert()->success('شما با موفقیت وارد شدید','ورود موفق');
    }

    protected function loggedOut(Request $request)
    {
        alert()->success('شما با موفقیت خارج شدید','خروج موفق');

    }


}
