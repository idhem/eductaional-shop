<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('status')) {
            $status = $request->get('status');
            $payments = Payment::latest()->where('status',$status)->paginate(20);

        } else {
            $payments = Payment::latest()->paginate(20);
        }

        return view('Admin.payments.all',compact('payments'));

    }
}
