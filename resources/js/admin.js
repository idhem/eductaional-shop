

/*
* Admin panel JS requirements
* */

import jquery from 'jquery';
import swal from 'sweetalert';
window.jQuery = jquery;
window.$ = jquery;
window.jquery = jquery;

require('./bootstrap.bundle.min');

require('./admin/persian-date.min');

require('./admin/persian-datepicker.min');

require('./admin/plugins/chart');

require('./admin/adminlte.min');

require('./sheard/bootstrap-select.min');


$(document).ready(function () {
    $('select').selectpicker();
});
