
<div class="cat-sidebar">
    <div class="w-100 text-center mt-3">
        <a class="cat-sidebar-btn text-light "> <i class="material-icons" style="vertical-align: -3px">close</i>بستن منو </a>
    </div>
    <div class="cat-list">
        <div>
            @foreach($categories as $category)
                <a href="{{ route('home.categories')  }}?name={{$category->label}}">{{$category->name}}</a>
                @foreach($category->categories as $cat)
                    <div class="mr-3 d-flex flex-column">
                        <a href="{{ route('home.categories')  }}?name={{$cat->label}}"> {{$cat->name}}</a>
                    </div>
                @endforeach
            @endforeach
        </div>
    </div>
</div>

