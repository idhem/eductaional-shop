<nav class="navbar navbar-expand-md navbar-dark" style="background: #37474F">
    <span class="text-light cat-sidebar-btn  ml-4" style="position: absolute"> دسته بندی ها  <i style="font-size: 18px;vertical-align: -2px" class="material-icons">menu</i> </span>
    <div class="container searchbox">
        <div class="input-group input-group-lg">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="material-icons" style="font-size: 22px">search</i></span>
            </div>
            <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">

            <div class="searchbox-container">
                <div>
                    <a href="">slam</a>
                </div>
            </div>
        </div>
    </div>
</nav>

<div class="navbar navbar-expand-md bg-light">
 <div class="container">
     <a class="navbar-brand" href="/">آموز کده</a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
     </button>

     <div class="collapse text-dar navbar-collapse" id="navbarsExampleDefault">

         <ul class="navbar-nav mr-4 ml-auto">
             <li class="nav-item active">
                 <a class="nav-link" href="/">خانه <span class="sr-only">(current)</span></a>
             </li>
             <li class="nav-item active">
                 <a class="nav-link" href="{{ route('home.article.all') }}">مقالات</a>
             </li>
             <li class="nav-item active">
                 <a class="nav-link" href="{{ route('home.course.all') }}"> دوره ها </a>
             </li>
             @if(auth()->check())
                 <li class="nav-item">
                     <a class="nav-link" href="{{route('user.profile',user()->id)}}" tabindex="-1" aria-disabled="true">پروفایل</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="/logout" tabindex="-1" aria-disabled="true">خروج</a>
                 </li>
                 @if(user()->level === 'admin')
                     <li class="nav-item">
                         <a class="nav-link" href="/admin" tabindex="-1" aria-disabled="true">پنل مدیریت</a>
                     </li>
                 @endif
             @else
                 <li class="nav-item">
                     <a class="nav-link" href="{{route('login')}}">ورود  </a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="{{route('register')}}"> ثبت نام</a>
                 </li>
             @endif
         </ul>
     </div>
 </div>
</div>

@include('section.sidebar')

