<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'اطلاعات شما تطابق نداره.',
    'throttle' => 'تلاش زیاد برای ورود.لطفا چند دقیقه دیگر دوباره تلاش کنید.',
    'login_title' => 'ورود به وبسایت',
    'email' => 'رایانامه',
    'password' => 'گذرواژه',
    'remember' => 'گذرواژه',
    'login_btn' => 'ورود',
    'login_btn_google' => 'ورود با اکانت گوگل',
    'forget_password' => 'گذرواژه خود را فراموش کرده اید؟',
    'register_title' => 'ثبت نام در آموزکده',
    'name' => 'نام',
    'confirm_password' => 'تکرار گذرواژه',
    'register_btn' => 'ثبت نام',


];
