@extends('Admin.master')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">کاربران</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('roles.index')}}" class="btn btn-primary" disabled>سطوح مدیرتی</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">
        @include('Admin.section.flash')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">لیست کاربران</h3>
            </div>
            <!-- /.card-header -->

            @if($users->count() > 0)
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>برچسب</th>
                            <th>تنظیمات</th>
                        </tr>
                        @foreach($users as $key => $user)
                            <tr>
                                <td>{{$key + 1}}.</td>
                                <td>{{ $user->name  }}</td>
                                <td>
                                    {{$user->email}}
                                </td>
                                <td>
                                    <form action="{{route('users.destroy',$user->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            @if($user->level == 'user')
                                            <a href="{{route('users.levelChange',$user->id)}}" class="btn btn-sm btn-info btn-flat">
                                                    اعمال دسترسی
                                            </a>
                                            @else
                                                <a href="{{route('users.rolesIndex',$user->id)}}" class="btn btn-sm btn-primary btn-flat">
                                                    انتساب نقش
                                                </a>
                                                <a href="{{route('users.levelChange',$user->id)}}" class="btn btn-sm btn-warning btn-flat">
                                                گرفتن دسترسی
                                                </a>

                                            @endif
                                            <button type="submit" class="btn btn-sm btn-danger btn-flat"><span class="material-icons">delete</span></button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody></table>
                </div>
                {{$users->render()}}
            @else
                <div class="callout callout-warning m-4" role="alert">
                    موردی یافت نشد
                </div>
        @endif

        <!-- /.card-body -->
        </div>
    </div>


@endsection
