<?php

namespace App\Http\Controllers;

use App\Category;
use App\Episode;
use App\Payment;
use foo\bar;
use GuzzleHttp\Client;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Course;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Expr\Cast\Object_;
use SEO;

class CourseController extends Controller
{
    public function single(Course $course)
    {
        SEO::setTitle($course->title);
        $course->increment('viewCount');
        $comments = $course->comments()->with('comments', 'user')->latest()->get();
        return view('Web\courses\single', compact('course', 'comments'));
    }

    public function all()
    {
        SEO::setTitle('دوره های وبسایت آموزکده');
        $courses = null;
        $courseCategory = null;
        if(Request()->get('category')) {
            $courses = Category::whereLabel(Request('category'))->first()->courses()->latest()->paginate(16);
            $courseCategory = $courses->first()->categories()->whereLabel(Request('category'))->first();
        }else {
            $courses = Course::latest()->paginate(16);
        }
        return view('Web.courses.all',compact('courses','courseCategory'));
    }


    public function payment(Request $request)
    {
        $course = Course::findOrFail($request->input('course_id'));

        if (!$this->checkPaymentRules($course)) return back();

        $paymentData = [
            "api" => "test",
            "amount" => $course->price,
            "redirect" => "http://localhost:8000/payment/callback",
            "mobile" => "09175353473",
            "factorNumber" => $course->id
        ];

        $client = new Client([
            'base_uri' => 'https://pay.ir'
        ]);

        $response = $client->post('/pg/send', [
            'json' => $paymentData
        ])->getBody();
        $response = json_decode($response);

        $userPayment = auth()->user()->payments();
        if (!count($userPayment->where('course_id', $course->id)->get())) {
            $userPayment->create([
                'course_id' => $course->id
            ]);
        }


        return redirect("https://pay.ir/pg/{$response->token}");
    }

    public function paymentCallback()
    {
        if (Request('status') === "1") {

            $paymentData = [
                "api" => "test",
                "token" => Request("token"),
            ];

            $client = new Client([
                'base_uri' => 'https://pay.ir'
            ]);

            $response = $client->post('/pg/verify', [
                'json' => $paymentData
            ])->getBody();
            $response = json_decode($response);
            $course = Course::find($response->factorNumber)->first();

            if (!$response->status) {
                alert()->error($response->errorMessage, 'پرداخت شما با موفقیت انجام نشده!خطا');
                return redirect($course->path());
            }

            $userPayment = auth()->user()->payments()->where('course_id', $response->factorNumber)->first();

            if ($response->amount != $userPayment->course->price) {
                alert()->error("مبلغ پرداختی با دوره مورد نظر مطابقت ندارد", '!خطا');
                return redirect($course->path());
            }
            $userPayment->update([
                "status" => 1,
                "trans_number" => $response->transId
            ]);

            alert()->success("پرداخت شما با موفقیت انجام شد!", '!عملیات موفق', 'با تشکر');

            return redirect($course->path());
        }
        alert()->error('انصراف شما', 'پرداخت شما با موفقیت انجام نشده!خطا');
        return redirect(route('home'));
    }

    private function checkPaymentRules(Course $course)
    {
        if ($course->type !== 'cash') {
            alert()->error('این درخواستی شما از نوع نقدی نمی باشد', 'خطایی صورت داده!');
            return false;
        }
        if ($course->price === '0') {
            alert()->error('این دوره قابل پرداخت نمی باشد ', 'خطایی صورت داده!');
            return false;
        }

        if (Payment::isLearning($course->id)) {
            alert()->warning('شما قبلا در این دوره ثبت نام کرده اید !', 'اخطار');
            return false;
        }

        return true;
    }

    public function downloadVideo(Episode $episode)
    {
        $status = false;
        switch ($episode->course->type) {
            case 'free' :
                $status = true;
                break;
            case 'vip' :
                auth()->user->type == 'vip' ? $status = true : $status = false;
                break;
            case 'cash' :
                $status = Payment::isLearning($episode->course->id);
                break;
        }
        if (!$status) {
            return abort(404, 'cannot found any video');
        }
        $chekedHash = Hash::check(
            "hawgaw32[ro32aF&9gikpZ3!1aw;ik]qw" . \Request()->ip() . Request()->get('t'),
            Request()->get('mac')
        );

        if (!$chekedHash) abort(400, 'hash code not correct');

        return Response()->download(storage_path($episode->videoUrl));
    }
}
