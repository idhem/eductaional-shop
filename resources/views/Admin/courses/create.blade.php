@extends('Admin.master')

@section('scripts')
    <script src="/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('body',{
            filebrowserBrowseUrl: '/admin/panel/fileUpload?&_token={{csrf_token()}}',
            filebrowserUploadUrl: '/admin/panel/fileUpload?&_token={{csrf_token()}}'
        });
    </script>

@endsection

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">مقالات</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('articles.index')}}" class="btn btn-secondary">لیست مقالات</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">

        @include('Admin.section.errors')

        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">افزودن دوره</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->

            <form action="{{route('courses.store')}}" method="post" enctype="multipart/form-data" role="form">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">موضوع</label>
                        <input type="text" name="title" class="form-control" id="title" value="{{old('title')}}" placeholder="موضوع را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="title">نوع دوره</label>

                        <select class="form-control selectpicker" name="type">
                            <option value="free" selected>رایگان</option>
                            <option value="vip">ویژه</option>
                            <option value="cash">نقدی</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">دسته بندی های مربوطه</label>

                        <select class="form-control selectpicker"  data-live-search="true" name="categories[]" multiple>
                        @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">انتخاب عکس</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="images" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">انتخاب عکس مقاله</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Upload</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="body">متن اصلی</label>
                        <textarea  class="form-control" name="body" id="body" rows="5" >{{old('body')}}</textarea>
                    </div>
                    <div class="form-group">

                        <div class="row">
                            <div class="col-lg-6 col-sm-12">
                                <label for="price">قیمت</label>
                                <input type="text" name="price" class="form-control" id="price" value="{{old('price',0)}}" placeholder="قیمت را وارد کنید">
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <label for="tags">تگ ها</label>
                                <input type="text" name="tags" class="form-control" id="tags" value="{{old('tags')}}" placeholder="تگ ها را وارد کنید">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">ارسال</button>
                </div>
            </form>
        </div>
    </div>

@endsection
