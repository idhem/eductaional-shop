<?php

namespace App\Http\Controllers;

use DebugBar\DebugBar;
use Illuminate\Http\Request;
use App\Article;
use SEO;

class ArticleController extends Controller
{
    public function single(Article $article)
    {
        SEO::setTitle($article->title);
        $article->increment('viewCount');
        $comments = $article->comments()->with('comments', 'user')->latest()->get();
        return view('Web\articles\single', compact('article', 'comments'));
    }

    public function all()
    {
        SEO::setTitle('مقالات وبسایت آموزکده');
        $articles = Article::latest()->paginate(10);
        return view('Web.articles.all',compact('articles'));
    }
}
