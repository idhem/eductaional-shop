@component('Web\profile\master')
<form action="{{ route('user.profile.vip.payment') }}" method="post">
  @csrf

  <div class="input-group mt-3 mb-3">
    <div class="input-group-prepend">
      <label class="input-group-text" for="vipPlan">پلن ها</label>
    </div>
    <select name="vipType" id="vipPlan">
      <option selected>انتخاب کنید...</option>
      <option value="1">۱ ماهه</option>
      <option value="3">۳ ماهه</option>
      <option value="12">۱۲ ماهه</option>
    </select>

    <button class="btn btn-primary mr-2" type="submit">ارتقاع</button>
  </div>
</form>
@endcomponent