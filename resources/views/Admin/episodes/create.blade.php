@extends('Admin.master')

@section('scripts')
    <script src="/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('body',{
            filebrowserBrowseUrl: '/admin/panel/fileUpload?&_token={{csrf_token()}}',
            filebrowserUploadUrl: '/admin/panel/fileUpload?&_token={{csrf_token()}}'
        });
    </script>

@endsection

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">ویدئو ها</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('episodes.index')}}" class="btn btn-secondary">لیست ویدئوها</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">

        @include('Admin.section.errors')

        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">افزودن ویدئو</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->

            <form action="{{route('episodes.store')}}" method="post" enctype="multipart/form-data" role="form">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">موضوع</label>
                        <input type="text" name="title" class="form-control" id="title" value="{{old('title')}}" placeholder="موضوع را وارد کنید">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="title">نوع ویدئو</label>

                                <select class="form-control selectpicker" name="type">
                                    <option value="free" selected>رایگان</option>
                                    <option value="vip">ویژه</option>
                                    <option value="cash">نقدی</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="title">دوره مورد نظر</label>

                                <select class="form-control selectpicker" name="course_id">
                                    @foreach ($courses as $course)
                                        <option value="{{$course->id}}">{{$course->title}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="body">متن اصلی</label>
                        <textarea  class="form-control" name="body" id="body" rows="5" >{{old('body')}}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group" dir="ltr" >
                                <label for="body">آدرس ویدئو</label>
                                <input type="text" name="videoUrl" class="form-control text-left" id="tags" value="{{old('videoUrl')}}" placeholder="زمان را وارد کنید">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="body">شماره ویدئو</label>
                                <input type="text" name="number" class="form-control" id="tags" value="{{old('number')}}" placeholder="زمان را وارد کنید">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="body">زمان ویدئو</label>
                                <input type="text" name="time" class="form-control" id="tags" value="{{old('time')}}" placeholder="زمان را وارد کنید">
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="tags">تگ ها</label>
                                <input type="text" name="tags" class="form-control" id="tags" value="{{old('tags')}}" placeholder="تگ ها را وارد کنید">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">ارسال</button>
                </div>
            </form>
        </div>
    </div>

@endsection
