<?php

namespace App\Http\Controllers;

use App\User;
use App\Payment;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {   
        return view('Web\profile\index');
    }

    public function history()
    {
        $history = Payment::whereId(user()->id)->with('user','course')->paginate();

        return view('Web\profile\history',compact('history'));
    }

    public function vip()
    {   
        return view('Web\profile\vip');
    }

    public function payment(Request $request)
    {
        switch($request->vipType) {
            case 3 :
            $price = 600000;
            case 6 : 
            $price = 1000000;
            case 12 :
            $price = 160000;
            default : 
            $price = 300000;
        }

        $paymentData = [
            "api" => config('app.payment.secret'),
            "amount" => $price,
            "redirect" => route('user.profile.vip.payment.callback'),
            "mobile" => "09175353473",
        ];

        $client = new Client([
            'base_uri' => 'https://pay.ir'
        ]);

        $response = $client->post('/pg/send', [
            'json' => $paymentData
        ])->getBody();
        $response = json_decode($response);


        return redirect("https://pay.ir/pg/{$response->token}");
    }

    public function paymentCallback()
    {
        if (Request('status') === "1") {

            $paymentData = [
                "api" => config('app.payment.secret'),
                "token" => Request("token"),
            ];

            $client = new Client([
                'base_uri' => 'https://pay.ir'
            ]);

            $response = $client->post('/pg/verify', [
                'json' => $paymentData
            ])->getBody();
            $response = json_decode($response);
            if (!$response->status) {
                alert()->error($response->errorMessage, 'پرداخت شما با موفقیت انجام نشده!خطا');
                return redirect(route('user.profile.vip'));
            }

            $month = 0;
            switch($response->amount) {
                case 600000:
                $month = 3;
                case 1000000 : 
                $month = 6;
                case 160000 :
                $month = 12;
                case 300000 : 
                $month = 1;
            }
           
            $currentVipTime = Carbon::parse(user()->vipTime);
            $vipDateTime = Carbon::now();
            if($currentVipTime->gt($vipDateTime)) {
                $vipDateTime = $currentVipTime;
            }
    
            User::whereId(user()->id)->update([
                'vipTime' => $vipDateTime->addMonths($month)
            ]);

            

            alert()->success("پرداخت شما با موفقیت انجام شد!", '!عملیات موفق', 'با تشکر');

            return redirect(route('user.profile'));
        } 
        alert()->error('انصراف شما', 'پرداخت شما با موفقیت انجام نشده!خطا');
        return redirect(route('user.profile.vip'));
        
    }
}
