<?php

namespace App\Http\Controllers\Api\v1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    function all()
    {
       $user = User::latest()->select('name','level','email')->get();
        return response([
            'data' => $user,
            'status' => 200
        ],200);
    }
}
