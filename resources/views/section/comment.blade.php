 <!-- Blog Comments -->

    <!-- Comments Form -->
    <div class="well">
        <h4>ثبت نظر :</h4>
        <form role="form" class="commentForm" id="comment">
            <div class="form-group">
                <input type="hidden" name="api_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="parent_id" value="0" />
                <input type="hidden" name="commentable_id" value="{{ $subject->id  }}" />
                <input type="hidden" name="commentable_type" value="{{ get_class($subject)  }}" />
                <textarea class="form-control" name="body" rows="3" required></textarea>
                <div class="invalid-feedback">
                    متن باید حتما وارد شود
                </div>
            </div>
            <button type="submit" class="btn btn-primary">ارسال</button>
        </form>
    </div>

    <hr>

    <!-- Posted Comments -->

    @foreach($comments as $comment)
        <!-- Comment -->
        <div class="media">
            <a class="pull-right" href="#">
                <img class="media-object" src="http://placehold.it/64x64" alt="">
            </a>
            <div class="media-body">
                <h4 class="media-heading">{{$comment->user->name}}
                    <small>{{ jdate($comment->created_at)->ago()  }}</small>
                    <button class="pull-left btn btn-xs btn-success" data-toggle="modal" data-target="#sendCommentModal" data-parent="{{$comment->id}}">پاسخ</button>
                </h4>
                {{$comment->body}}
                <!-- Nested Comment -->
               @foreach($comment->comments as $commentChild)
                    <div class="media">
                        <a class="pull-right" href="#">
                            <img class="media-object" src="http://placehold.it/64x64" alt="">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading font-size-4"><a class="font-size-5">{{$commentChild->name}}</a>
                            </h4>
                            <small>{{ jdate($commentChild->created_at)->ago()  }}</small>
                            {{ $commentChild->body  }}
                        </div>
                    </div>
               @endforeach
                <!-- End Nested Comment -->
            </div>
        </div>
    @endforeach

    <div class="modal fade" id="sendCommentModal" tabindex="-1" role="dialog" aria-labelledby="sendCommentModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">ارسال پاسخ</h4>
                </div>
                <div class="modal-body">
                    <form class="commentForm" method="post">
                        <input type="hidden" name="api_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="parent_id" value="0" />
                        <input type="hidden" name="commentable_id" value="{{ $subject->id  }}" />
                        <input type="hidden" name="commentable_type" value="{{ get_class($subject)  }}" />
                        <div class="form-group">
                            <label for="message-text" class="control-label">متن پاسخ:</label>
                            <textarea class="form-control" name="body"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">ارسال</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
