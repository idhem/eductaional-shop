<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelController extends AdminController
{
    public function index()
    {
        return view('Admin.dashboard');
    }

    public function uploadImageSubject()
    {

        $this->validate(Request(),[
            'upload' => 'required|mimes:jpeg,bmp,png'
        ]);

        $file = Request()->file('upload');
        $now = Carbon::now();
        $address = "/upload/images/{$now->year}/{$now->month}/";
        $filename = $now->timestamp . '-' . $file->getClientOriginalName();

        $file->move(public_path($address) , $filename);
        $url = $address . $filename;
        return "<script>window.parent.CKEDITOR.tools.callFunction(1 , '{$url}' , ' فایل با موفقیت اپلود شد ')</script>";
    }
}
