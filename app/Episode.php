<?php

namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Hash;

/**
 * App\Episode
 *
 * @property int $id
 * @property int $course_id
 * @property string $type
 * @property string $title
 * @property string $description
 * @property string $body
 * @property string $videoUrl
 * @property string $time
 * @property string $slug
 * @property string $tags
 * @property int $number
 * @property int $viewCount
 * @property int $commentCount
 * @property int $downloadCount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read \App\Course $course
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereCommentCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereDownloadCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereVideoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Episode whereViewCount($value)
 * @mixin \Eloquent
 */
class Episode extends Model
{
    protected $guarded = [];
    use Sluggable;

    protected $casts = [
        'images' => 'array'
    ];
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function course() {
        return $this->belongsTo(Course::class);
    }

    public function setBodyAttribute($value)
    {
        $this->attributes['description'] = substr(preg_replace('/<[^>]*>/','',$value),0,200);
        $this->attributes['body'] = $value;
    }

    public function comments()
    {
        return $this->morphMany(Comment::class,'commentable');
    }

    public function download()
    {
        $status = false;
        if(!auth()->check()) {
            return route('login');
        }
        switch ($this->course->type) {
            case 'free' :
                $status = true;
            break;
            case 'vip' :
                auth()->user->type == 'vip' ? $status = true : $status = false;
            break;
            case 'cash' :
                $status = Payment::isLearning($this->course->id);
            break;
        }
        if(!$status) {
            return '#';
        }
        $timestamp = Carbon::now()->addHour(5)->timestamp;
        $hash = Hash::make("hawgaw32[ro32aF&9gikpZ3!1aw;ik]qw".\Request()->ip().$timestamp);
        $link = "/download/{$this->id}?mac={$hash}&t={$timestamp}";
        return $link;
    }
}
