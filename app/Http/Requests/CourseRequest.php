<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'type' => 'required',
                    'title' => 'required',
                    'body' => 'required|min:5',
                    'price' => 'required',
                    'images' => 'required|mimes:jpeg,bmp,png,jpg',
                    'tags'=> 'required'
                ];
            case 'PUT':
                return [
                    'type' => 'required',
                    'title' => 'required',
                    'body' => 'required|min:5',
                    'price' => 'required',
                    'tags'=> 'required'
                ];
        }

    }
}
