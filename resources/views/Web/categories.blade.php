@extends('master')


@section('content')

    <div class="row">
        <div class="col-lg-12 d-flex">
            <h3>آخرین دوره های مربوط به {{ $category->name }}</h3>
            <div class="mt-1 mr-2">
                <a class="btn-sm btn btn-info" href="{{ route('home.course.all') }}?category={{$category->label}}">
                    مشاهده بیشتر در این مورد
                </a>
            </div>
        </div>
    </div>
    <div class="row ">

        @foreach ($courses as $course)
            <div class="col-md-3  col-sm-6 hero-feature">
                <div class="thumbnail card">
                    <div style="height: 215px;overflow: hidden">
                        <img class="w-100"  src="{{ $course->images['480'] }}" alt="{{ $course->title }}">
                    </div>
                    <div class="caption mx-2">
                        <h3><a class="font-size-7" href="{{ $course->path() }}">{{ $course->title }}</a></h3>
                        <p class="font-size-5"> {!! str_limit(strip_tags($course->body),120) !!} </p>
                        <p>
                            <a href="{{ $course->path() }}" class="btn btn-primary">خرید</a> <a href="{{ $course->path() }}" class="btn btn-default">اطلاعات بیشتر</a>
                        </p>
                    </div>
                    <div class="ratings">
                        <span class="pull-left pb-4 pr-3">{{ $course->viewCount }} بازدید</span>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <hr>

    <div class="row">
        <div class="col-sm-12 d-flex">
            <h3>آخرین مقالات مربوط به {{ $category->name }}</h3>
            <div class="mt-1 mr-2">
                <a class="btn-sm btn btn-info" href="{{ route('home.article.all') }}?category={{$category->label}}">
                    مشاهده بیشتر در این مورد
                </a>
            </div>
        </div>
        @foreach ($articles as $article)
            <div class="col-sm-4 col-lg-4 col-md-4">
                <div class="thumbnail card">
                    <div style="height: 215px;overflow: hidden">
                        <img class="w-100" src="{{ $article->images['480'] }}" alt="{{ $article->title }}">
                    </div>
                    <div class="caption mx-2">
                        <h4><a class="font-size-7" href="{{ $article->path() }}">{{ $article->title }}</a>
                        </h4>
                        <p class="font-size-5">  {!! str_limit(strip_tags($article->body),140) !!} </p>
                    </div>
                    <div class="ratings">
                        <p class="pull-right">{{ $article->viewCount }} بازدید</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
