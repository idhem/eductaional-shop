<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::whereEmail('admin@gmail.com')->count() > 0) return;
        User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'level' => 'admin',
            'email_verified_at' => now(),
            'password' => Hash::make('1234'), // password
            'remember_token' => Str::random(10),
        ]);
    }
}
