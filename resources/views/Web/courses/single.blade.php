@extends('master')


@section('content')
    <div class="row mt-3">
             <!-- Blog Post Content Column -->
    <div class="col-lg-8 ">

        <!-- Blog Post -->

        <!-- Title -->
        <h1>{{ $course->title }}</h1>

        <!-- Author -->
        <p class="lead small">
            توسط <a href="#">{{ $course->user->name }}</a>
        </p>

        <hr>

        <!-- Date/Time -->
        <p><span class="glyphicon glyphicon-time"></span> ارسال شده در {{ jdate($course->created_at)->format('%d %B %y') }}</p>

        <hr>
        <div>
            {!! $course->body !!}
        </div>
        <hr>
       @if (Auth()->user())
        @if ($course->type == 'cash')
        <div class="alert alert-danger" role="alert">برای مشاهده تمامی قسمت ها باید این دوره را خریداری کنید</div>
        @elseif($course->type == 'vip')
        <div class="alert alert-danger" role="alert">برای مشاهده تمامی قسمت ها باید عضویت ویژه تهیه کنید</div>
        @endif

       @else
       <div class="alert alert-danger" role="alert">برای دسترسی به دوره وارد اکانت خود شوید</div>           
       @endif


        <h3>قسمت های دوره</h3>
        <table class="table table-condensed table-bordered">
            <thead>
                <tr>
                    <th>شماره قسمت</th>
                    <th>عنوان قسمت</th>
                    <th>زمان قسمت</th>
                    <th>دانلود</th>
                </tr>
            </thead>
            <tbody>
            @foreach($course->episodes as $key => $episode)
                <tr>
                    <th scope="row">{{$key}}</th>
                    <td>{{$episode->title}}</td>
                    <td>{{$episode->time}}</td>
                    <td>
                        <a href="{{ $episode->download() }}"><span class="glyphicon glyphicon-download-alt" aria-hidden="true">لینک دانلود</span></a>
                    </td>
                </tr>
            @endforeach




            </tbody> </table>
        <!-- Blog Comments -->


        <!-- Posted Comments -->


        @include('section\comment',['subject'=> $course,'comments' => $comments])

    </div>

    <!-- Blog Sidebar Widgets Column -->
    <div class="col-lg-4 col-md-4">
        <div class="well card p-3">
            برای استفاده از این دوره نیاز است این دوره را با مبلغ {{ $course->price }} تومان خریداری کنید
            <form method="POST" action="{{route('payment.course')}}">
                @csrf
                <input type="hidden" name="course_id" value="{{$course->id}}"/>
                <button class="btn btn-success">خرید دوره</button>
            </form>
        </div>
        <!-- Blog Search Well -->
        <div class="well mt-3">
            <h4>جستجو در سایت</h4>
            <div class="input-group">
                <input type="text" class="form-control">
                <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
            </div>
            <!-- /.input-group -->
        </div>

        <!-- Side Widget Well -->
        <div class="well">
            <h4>دیوار</h4>
            <p>طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد.</p>
        </div>

    </div>
    </div>


@endsection
