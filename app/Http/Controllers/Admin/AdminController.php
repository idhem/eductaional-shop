<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Episode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;


class AdminController extends Controller
{
    protected function uploadImage(UploadedFile $file)
    {
        $now = Carbon::now();
        $filePath = '/upload/images/' . $now->year . '/' . $now->month . '/';
        $filename = $now->timestamp . '.' . $file->getClientOriginalName();
        $sizes = [480, 360, 240];
        $fileSource = Image::make($file->getRealPath());
        $file->move(public_path($filePath), $filename);
        $images = [
            'orginal' => $filePath . $filename
        ];
        foreach ($sizes as $size) {
            $fileAddress = $filePath . $size . '_' . $filename;
            $fileSource->resize(null, $size, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path($fileAddress));
            $images[$size] = $fileAddress;
        }
        return $images;
    }

    /**
     * @param  $request
     * @param $state
     * @param string $lastEpisodeTime
     * */
    protected function setCourseTime($request , $state = 1,$lastEpisodeTime = null)
    {
        $course = Course::all()->find($request->course_id);
        if($state == 1)
            $course->time = $this->increaseTime($course->time,$request->time);
        else if($state === 2)
            $course->time = $this->decreaseTime($course->time,$request->time);
        else if ($state === 3) {
            $course->time = $this->decreaseTime($course->time,$lastEpisodeTime);
            $course->time = $this->increaseTime($course->time,$request->time);
        }
       $course->save();
    }

    private function increaseTime(string $firstTime,string $secondTime)
    {
       $time = Carbon::parse($firstTime);
       $time->addSecond(strlen($secondTime) === 5 ? strtotime("00:$secondTime") : strtotime($secondTime));
       return $time->format('H:i:s');
    }
    private function decreaseTime(string $firstTime,string $secondTime)
    {
        $time = Carbon::parse($firstTime);
        $time->subSecond(strlen($secondTime) === 5 ? strtotime("00:$secondTime") : strtotime($secondTime));
        return $time->format('H:i:s');
    }
}
