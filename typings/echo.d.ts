
import Echo from 'node_modules/laravel-echo/dist/echo';

declare module 'Echo' {
    var Echo: Echo ;
    export = Echo;
}

