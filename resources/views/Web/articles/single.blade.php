@extends('master')


@section('content')
       <div class="row mt-3">
 <!-- Blog Post Content Column -->
 <div class="col-lg-8">

    <!-- Blog Post -->

    <!-- Title -->
    <h1 style="font-size:24px">{{ $article->title }}</h1>

    <!-- Author -->
    <p class="lead">
        توسط <a href="#">{{ $article->user->name }}</a>
    </p>

    <hr>

    <!-- Date/Time -->
    <p><span class="glyphicon glyphicon-time"></span> ارسال شده در   {{ jdate($article->created_at)->format('%d %B %y') }}</p>

    <hr>

    <!-- Preview Image -->
  <div>
    <img class="w-100" class="img-responsive" src="{{ $article->images['480'] }}" alt="">
  </div>

    <hr>

    <!-- Post Content -->
   
   {!! $article->body !!}
   
    <hr>

   @include('section\comment',['subject'=> $article,'comments' => $comments])

</div>

<!-- Blog Sidebar Widgets Column -->
<div class="col-md-4">

    <!-- Blog Search Well -->
    <div class="well">
        <h4>جستجو در سایت</h4>
        <div class="input-group">
            <input type="text" class="form-control">
            <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <span class="glyphicon glyphicon-search"></span>
                </button>
                </span>
        </div>
        <!-- /.input-group -->
    </div>

    <!-- Side Widget Well -->
    <div class="well">
        <h4>دیوار</h4>
        <p>طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد.</p>
    </div>

</div>
       </div>

   
@endsection
