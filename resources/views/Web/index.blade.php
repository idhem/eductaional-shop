@extends('master')


@section('content')
    <!-- Jumbotron Header -->
    <header class="jumbotron mt-2 hero-spacer bg-home-titr">
        <h1>به آموزکده خوش آمدید</h1>
        <p class="text-justify">معمولا طراحان گرافیک برای صفحه‌آرایی، نخست از متن‌های آزمایشی و بی‌معنی استفاده می‌کنند تا صرفا به مشتری یا صاحب کار خود نشان دهند که صفحه طراحی یا صفحه بندی شده بعد از اینکه متن در آن قرار گیرد چگونه به نظر می‌رسد و قلم‌ها و اندازه‌بندی‌ها چگونه در نظر گرفته شده‌است. از آنجایی که طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برند.</p>
        </p>
    </header>

    <h2 class="w-100 text-center " style="color: #33333d"> با ما در همه زمینه ها متخصص باشید </h2>
    <div class="row my-5">
        <div class="col-lg-3">
            <div class="d-flex justify-content-center flex-column">
                <div class="text-center">
                    <img src="/images/artificial-intelligence.png" />
                </div>
                <h5 class="text-center mt-4" style="color: #565661"> هوش مصنوعی </h5>
            </div>

        </div>
        <div class="col-lg-3">
            <div class="d-flex justify-content-center flex-column">
                <div class="text-center">
                    <img src="/images/coding.png" />
                </div>
                <h5 class="text-center mt-4" style="color: #565661"> برنامه نویسی وب </h5>
            </div>

        </div>

        <div class="col-lg-3">
            <div class="d-flex justify-content-center flex-column">
                <div class="text-center">
                    <img src="/images/mobile-app.png" />
                </div>
                <h5 class="text-center mt-4" style="color: #565661"> برنامه نویسی موبایل </h5>
            </div>

        </div>
        <div class="col-lg-3">
            <div class="d-flex justify-content-center flex-column">
                <div class="text-center">
                    <img src="/images/risks.png" />
                </div>
                <h5 class="text-center mt-4" style="color: #565661"> مدیریت پروژه </h5>
            </div>

        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-lg-12">
            <h3>آخرین دوره ها</h3>
        </div>
    </div>
    <div class="row ">

      @foreach ($courses as $course)
        <div class="col-md-3  col-sm-6 hero-feature">
            <div class="thumbnail card">
                <div style="height: 215px;overflow: hidden">
                    <img class="w-100"  src="{{ $course->images['480'] }}" alt="{{ $course->title }}">
                </div>
                <div class="caption mx-2">
                    <h3><a class="font-size-7" href="{{ $course->path() }}">{{ $course->title }}</a></h3>
                    <p class="font-size-5"> {!! str_limit(strip_tags($course->body),120) !!} </p>
                    <p>
                        <a href="{{ $course->path() }}" class="btn btn-primary">خرید</a> <a href="{{ $course->path() }}" class="btn btn-default">اطلاعات بیشتر</a>
                    </p>
                </div>
                <div class="ratings">
                    <span class="pull-left pb-4 pr-3">{{ $course->viewCount }} بازدید</span>
                </div>
            </div>
        </div>
      @endforeach
    </div>
    <hr>

    <div class="col-md-12 mb-5">
        <div class="row">
            <div class="col-sm-12">
                <h3>اخرین مقالات</h3>
            </div>
           @foreach ($articles as $article)
           <div class="col-sm-4 col-lg-4 col-md-4">
            <div class="thumbnail card">
                <div style="height: 215px;overflow: hidden">
                    <img class="w-100" src="{{ $article->images['480'] }}" alt="{{ $article->title }}">
                </div>
                <div class="caption mx-2">
                    <h4><a class="font-size-7" href="{{ $article->path() }}">{{ $article->title }}</a>
                    </h4>
                    <p class="font-size-5">  {!! str_limit(strip_tags($article->body),140) !!} </p>
                </div>
                <div class="ratings">
                    <p class="pull-right">{{ $article->viewCount }} بازدید</p>
                </div>
            </div>
        </div>
           @endforeach
        </div>
    </div>
@endsection
