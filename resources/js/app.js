
import jquery from 'jquery';

import _ from 'lodash';
import swal from 'sweetalert';


import Echo from 'laravel-echo';


window.jQuery = jquery;
window.$ = jquery;
window.jquery = jquery;

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap')
require('./bootstrap.bundle.min');
require('./shards.min');
require('./sheard/bootstrap-select.min');
window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).ready(function () {
    $('select').selectpicker();
});

window.Echo.channel('articles').listen('ArticleEvent',function (e) {
    console.log(e);
    console.log('ok');
});



$('.commentForm').on('submit',function (e) {
    e.preventDefault();
    let $this = $(this);
    let textarea = $this.find('textarea[name="body"]');
    let commentData = {
        commentable_id : $this.find('input[name="commentable_id"]').val(),
        commentable_type : $this.find('input[name="commentable_type"]').val(),
        body : textarea.val(),
    };

    $.ajax({
        method : "POST",
        url : '/comments',
        data : commentData
    }).done(function (response) {
       if (response.status === 'ok') {
           swal({
               title: "کامنت شما ثبت شد.",
               text: "منتظر تایید آن باشید.",
               icon: "info",
               button: "حتما!",
           });
           $this.find('textarea[name="body"]').val('');
       }
    });
});


$(document).ready(function () {
    $('.commentForm textarea[name="body"]').keyup(function () {
        let $this = $(this);
        if ($this.val().length === 0)
            $this.addClass('is-invalid');
        else
            $this.removeClass('is-invalid');
    });

    $('.cat-sidebar-btn').on('click',function () {
        var catSidebar = $('.cat-sidebar');
        if(catSidebar.css('display') === 'none') {
            catSidebar.css('display','block');
            catSidebar.animate({
                right : '0',
                opacity:'1'
            });
        } else {

            catSidebar.animate({
                right : '-250px',
                opacity:'0'
            },300, function () {
                catSidebar.css('display','none');
            });
        }

    });

    $('.searchbox input').on('keyup',function (e) {
        let searchText = $(this).val();
        if(searchText.length < 3) {
            if($('.searchbox-container').css('display')!=='none') {
                $('.searchbox-container').css('display','none');
            }
            return;
        };
        $('.searchbox-container').css('display','flex');
        $('.searchbox-container').html(`
        <div class="d-flex justify-content-between w-100">
        <strong>درحال بارگذاری...</strong>
        <div class="spinner-grow spinner-border-sm" role="status" aria-hidden="true"></div>
</div>
        `);
        $.ajax({
            url : '/search',
            method : 'POST',
            data : {
                text : searchText
            },
            success : function (response) {
                
                if(response.status === 'OK') {
                    searchResponseHandler(response.data,searchText);
                }            
            }
        });        
    });

});

/**
 * 
 * @param {Array} searchData
 * @param {string} searchText 
 */
function searchResponseHandler(searchData,searchText) {
    let doc = ``;
  for (const key in searchData) {
      if (searchData.hasOwnProperty(key)) {
          const element = searchData[key];
          element.map((data)=> {
            data.title = data.title.replace(searchText,`<b class="text-danger">${searchText}</b>`);
             doc += `
            <div>
                <a href="/${key}/${data.slug}">${data.title}</a>
            </div>`;
            data.document = doc;
            return data;
        });
    }
}
$('.searchbox-container').html(doc);
}
