@component('Web\profile\master')
<table class="table mt-2">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">مبلغ</th>
        <th scope="col">بابت</th>
        <th scope="col">وضعیت</th>
        <th scope="col">تاریخ پرداخت</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($history as $key => $payment)
    <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $payment->course->price }}</td>
        <td>{{ $payment->course->title }}</td>
        <td>{!! $payment->status ? '<b class="bg-success rounded text-white p-2">پرداخت شده</b>' : '<b class="bg-danger text-white rounded p-2">پرداخت نشده</b>' !!} </td>
        <td>{{ jdate($payment->course->created_at)->format('%d %B %y') }}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endcomponent