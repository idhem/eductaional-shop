@if(Session::has('message'))
    <div class="callout {{ Session::get('alert-class', 'callout-info')}}">
        <p class="alert ">{{ Session::get('message') }}</p>
    </div>
@endif
