@extends('Admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">دسته بندی ها</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('categories.index')}}" class="btn btn-secondary">لیست دسته بندی ها</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">

        @include('Admin.section.errors')

        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">افزودن مقاله</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->

            <form action="{{route('categories.store')}}" method="post" enctype="multipart/form-data" role="form">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">نام</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}" placeholder="نام را وارد کنید">
                    </div>


                    <div class="form-group">
                        <label for="tags">برچسب (انگلیسی)</label>
                        <input type="text" name="label" class="form-control" id="tags" value="{{old('label')}}" placeholder="برچسب  را وارد کنید">
                    </div>

                    <div class="form-group">
                        <label for="title">انتخاب سردسته </label>

                        <select type="number" class="form-control selectpicker" name="parent_id">
                            <option value="0">none</option>
                            @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach

                        </select>
                    </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">ارسال</button>
                </div>
            </form>
        </div>
    </div>

@endsection
