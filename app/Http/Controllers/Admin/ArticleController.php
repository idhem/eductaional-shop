<?php

namespace App\Http\Controllers\Admin;


use App\Article;
use App\Category;
use App\Events\ArticleNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ArticleController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::latest()->paginate(10);
        return view('Admin.articles.all' , compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('Admin.articles.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ArticleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {

        $iamgeUrl = $this->uploadImage($request->file('images'));

        $request['user_id'] = 1;
        $categories = $request->categories;
        unset($request['categories']);
        $article = Article::create(array_merge($request->all(),['images' => $iamgeUrl]));

        $article->categories()->attach($categories);

        event(new ArticleNotification($article));

        return redirect(route('articles.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $categories = Category::all();
        return view('Admin.articles.edit',compact('article','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $inputs = $request->all();
        $file = $request->file('images');
        if($file) {
            $inputs['images'] = $this->uploadImage($file);
        } else {
            $inputs['images'] = $article->images;
        }



       $article->update($inputs);


        Session::flash('message','ویرایش با موفقیت انجام شد');
        Session::flash('alert-class','callout-success');

        return redirect('/admin/articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->categories()->detach($article->id);
        Article::destroy($article->id);
        Session::flash('message','مقاله مورد نظر با موفقیت حذف شد');
        Session::flash('alert-class','callout-success');
        return back();
    }
}
