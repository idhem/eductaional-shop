<aside class="main-sidebar sidebar-light-warning elevation-3">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="/images/lock-open.png" width="32" alt="" srcset="">
        <span class="brand-text font-weight-light">پنل مدیریت</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    {{--<li class="nav-item has-treeview menu-close">
                        <a href="#" class="nav-link active">
                            <i class="material-icons">dashboard</i>
                            <p>
                                صفحات شروع
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="#" class="nav-link active">
                                    <i class="material-icons">check_circle_outline</i>
                                    <p>صفحه فعال</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>صفحه غیر فعال</p>
                                </a>
                            </li>
                        </ul>
                    </li>--}}
                    <li class="nav-item">
                        <a href="#" class="nav-link
                            @if( Request::getPathInfo() ==='/admin')
                            active
                            @endif">
                            <i class="material-icons">dashboard</i>
                            <p>
                                پیشخوان
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('articles.index')}}" class="nav-link
                            @if( Request::fullUrl() ===route('articles.index'))
                            active
                            @endif
                            ">
                            <i class="material-icons">description</i>
                            <p>
                                مقالات
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('courses.index')}}" class="nav-link
                            @if( Request::fullUrl() ===route('courses.index'))
                            active
                            @endif
                            ">
                            <i class="material-icons">library_books</i>
                            <p>
                               دوره ها
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('categories.index')}}" class="nav-link
                            @if( Request::fullUrl() ===route('categories.index'))
                            active
                            @endif
                            ">
                            <i class="material-icons">dynamic_feed</i>
                            <p> دسته بندی</p>
                        </a>
                    </li>
                    <li class="nav-item   has-treeview menu-close">
                        <a href="#" class="nav-link
                            @if( Request::fullUrl() === route('permissions.index') || 
                                 Request::fullUrl() === route('roles.index'))
                            active
                            @endif
                            ">
                            <i class="material-icons">vpn_key</i>
                            <p>
                            سطوح دسترسی
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('permissions.index') }}" class="nav-link active">
                                    
                                    <p>اجازه دسترسی</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('roles.index') }}" class="nav-link active">
                                    <p>نقش ها</p>
                                </a>
                            </li>
                        </ul>

                    </li>
                    <li class="nav-item">
                        <a href="{{route('comments.approvedIndex')}}" class="nav-link
                            @if( Request::fullUrl() ===route('comments.approvedIndex'))
                            active
                            @endif
                            ">
                            <i class="material-icons">chat_bubble</i>
                            <p>
                               کامنت ها
                               <span class="badge badge-pill badge-danger">{{ $newCommentCount }}</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('payments.index')}}" class="nav-link
                            @if( Request::fullUrl() ===route('payments.index'))
                            active
                            @endif
                            ">
                            <i class="material-icons">payment</i>
                            <p>
                              تراکنش ها
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
    </div>
    <!-- /.sidebar -->
</aside>
