<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;




Route::get('/', 'HomeController@index')->name('home');

Route::get('/courses/{courseSlug}', 'CourseController@single')->name('home.course.single');
Route::get('/courses','CourseController@all')->name('home.course.all');
Route::middleware(['auth'])->get('/download/{episode}','CourseController@downloadVideo');
Route::middleware(['auth'])->post('/payment/course','CourseController@payment')->name('payment.course');
Route::middleware(['auth'])->get('/payment/callback','CourseController@paymentCallback')->name('payment.callback');

Route::get('/articles/{articleSlug}', 'ArticleController@single')->name('home.article.single');
Route::get('/articles', 'ArticleController@all')->name('home.article.all');
Route::get('/contact-us', 'HomeController@contact')->name('home.contact');
Route::get('/categories', 'HomeController@categories')->name('home.categories');
Route::get('/about-us', 'HomeController@about')->name('home.about');
Route::post('/search', 'HomeController@searchAjax')->name('home.search.ajax');

Route::middleware(['auth'])->post('/comments','HomeController@storeComment')->name('home.comment.store');
Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth'],'prefix' => 'profile' , 'as' => 'user.'],function () {
    Route::get('/','ProfileController@index')->name('profile');
    Route::get('/history','ProfileController@history')->name('profile.history');
    Route::get('/vip','ProfileController@vip')->name('profile.vip');
    Route::post('/vip/payment','ProfileController@payment')->name('profile.vip.payment');
    Route::get('/vip/payment/callback','ProfileController@paymentCallback')->name('profile.vip.payment.callback');
});

Route::middleware(['auth','isUserAdmin','verified'])->namespace('Admin')->prefix('admin')->group(function () {


    //Articles Routes
    Route::resource('articles','ArticleController');

    // Courses Routes
    Route::get('/courses/episodes/{course}','CourseController@episodes')->name('courses.episodes');
    Route::resource('courses','CourseController');
    // Episodes Routes
    Route::resource('episodes','EpisodeController');

    // Categories Routes
    Route::resource('categories','CategoryController');

    // Roles And Permissions
    Route::resource('roles','RoleController');
    Route::resource('permissions','PermissionController');

    // Comments Routes
    Route::get('/comments/confirm/{comment}','CommentController@confirm')->name('comments.confirm');
    Route::get('/comments/approved','CommentController@approvedIndex')->name('comments.approvedIndex');
    Route::resource('comments','CommentController');

    // Payments Routes
    Route::get('/payments','PaymentController@index')->name('payments.index');

    // Others Routes
    Route::get('/','PanelController@index');
    Route::post('/panel/fileUpload','PanelController@uploadImageSubject');


    Route::group(['prefix' => 'users'],function () {
        Route::get('/','UserController@index')->name('users.all');
        Route::delete('/{user}','UserController@destroy')->name('users.destroy');
        Route::get('/{user}/levelChange','UserController@levelChange')->name('users.levelChange');
        Route::middleware('redirectIfLevelNotAdmin')->
        get('/{user}/roles','UserController@userRoleIndex')->name('users.rolesIndex');
        Route::middleware('redirectIfLevelNotAdmin')->
        post('/{user}/roles','UserController@userRole')->name('users.roles');
    });
});



Route::group([
    'namespace' => 'Auth'
],function () {
// Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');

    //login with google
    Route::get('login/google', 'LoginController@redirectToProvider')->name('login.google');
    Route::get('login/google/callback', 'LoginController@handleProviderCallback');
    // Registration Routes...
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');
    // RestPassword Routes...s
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');
    // Verify Email Routes...
    Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
    Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');



});

Route::get('/verify/googleRecaptcha','HomeController@verifyGoogleRecaptcha')->name('verification.recaptcha');

