@extends('Admin.master')



@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">اجازه ها</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('permissions.index')}}" class="btn btn-secondary">لیست اجازه ها</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">

        @include('Admin.section.errors')

        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">ویرایش اجازه</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->

            <form action="{{route('permissions.update',$permission->id)}}" method="post" role="form">
                @csrf
                   <div class="m-3">
                       <div class="row">
                           <div class="col-6">
                               <div class="form-group">
                                   <label for="body">نام </label>
                                   <input type="text" name="name" class="form-control" id="name" value="{{old('name',$permission->name)}}" placeholder="نام را وارد کنید">
                               </div>

                           </div>
                           <div class="col-6">
                               <div class="form-group">
                                   <label for="label"> برچسب </label>
                                   <input type="text" name="label" class="form-control" id="label" value="{{old('label',$permission->label)}}" placeholder="برچسب را وارد کنید">
                               </div>
                           </div>
                       </div>
                   </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">ارسال</button>
                </div>
            </form>
        </div>
    </div>

@endsection
