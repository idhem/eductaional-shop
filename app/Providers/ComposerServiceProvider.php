<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
       View::composer(['Web.profile.*','Web\profile\*'],function ($view) {
            $courrentRouteName = Route::current()->getName();
            debugbar()->addMessage($courrentRouteName);
            $view->with(compact('courrentRouteName'));
        });

        View::composer('Admin.*',function ($view) {
            $newCommentCount = \App\Comment::whereApproved('0')->count();
            $view->with(compact('newCommentCount'));
        });

        View::composer('section.nav',function ($view) {
            $categories = \App\Category::whereParentId(0)->latest()->get();
            $view->with(compact('categories'));
        });
    }
}
