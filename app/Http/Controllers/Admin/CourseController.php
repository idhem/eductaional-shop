<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Course;
use App\Episode;
use App\Http\Requests\ArticleRequest;
use App\Http\Requests\CourseRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CourseController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::latest()->paginate(10);
        return view('Admin.courses.all', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('Admin.courses.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        $imageUrl = $this->uploadImage($request->file('images'));
        $course = $request->all();
        $categories = [];
        if (isset($course['categories'])) {
            $categories = $course['categories'];
            unset($course['categories']);
        }
        $course = Auth::user()->
        Courses()->
        create(array_merge($course, ['images' => $imageUrl]));

        $course->categories()->attach($categories);

        return redirect(route('courses.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        $categories = Category::all();
        return view('Admin.courses.edit', compact('course', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  CourseRequest $course
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $request, Course $course)
    {
        $inputs = $request->all();
        $imageFile = $request->file('images');
        if ($imageFile) {
            $inputs['images'] = $this->uploadImage($imageFile);
        } else {
            $inputs['images'] = $course->images;
        }

        $categories = [];

        if (isset($inputs['categories'])) {
            $categories = $inputs['categories'];
            unset($inputs['categories']);
        }

        $course->update($inputs);

        $course->categories()->detach();
        $course->categories()->attach($categories);

        Session::flash('message', 'دوره مورد نظر با موفقیت ویرایش شد');
        Session::flash('alert-class', 'callout-success');
        return redirect(route('courses.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        Course::destroy($course->id);
        Session::flash('message', 'دوره مورد نظر با موفقیت حذف شد');
        Session::flash('alert-class', 'callout-success');
        return back();
    }

    public function episodes(Course $course)
    {
        $episodes = Episode::where('course_id', $course->id)->with('course')->latest()->paginate();
        return view('Admin.Episodes.all', compact('episodes'));
    }


}
