@extends('Admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="">
            <div class="d-flex justify-content-between mb-2">
                <div class="">
                    <h1 class="m-0 font-size-3 text-dark">دسته بندی ها</h1>
                </div><!-- /.col -->
                <div class="">
                    <a href="{{route('categories.create')}}" class="btn btn-primary">افزودن دسته بندی</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="container-fluid">
        @include('Admin.section.flash')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">دسته بندی</h3>
            </div>
            <!-- /.card-header -->

            @if($categories->count() > 0)
                <div class="card-body p-0">
                    <table class="table table-striped">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>تاریخ ارسال</th>
                            <th>سر دسته</th>
                            <th>برچسب</th>
                            <th>تنظیمات</th>
                        </tr>
                        @foreach($categories as $key => $category)
                            <tr>
                                <td>{{$key + 1}}.</td>
                                <td>{{ $category->name  }}</td>
                                <td>
                                    {{ jdate($category->created_at)->format('%A, %d %B %y')  }}
                                </td>
                                <td>
                                    @if($category->parent)
                                        {{$category->parent->name}}
                                    @else
                                        ندارد
                                    @endif
                                </td>
                                <td>{{ $category->label  }}</td>
                                <td>
                                    <form action="{{route('categories.destroy',['id' => $category->id])}}" method="post">
                                        @method('delete')
                                        @csrf
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{route('categories.edit',['id' => $category->id])}}" class="btn btn-sm btn-info btn-flat">
                                            <span class="material-icons">edit</span>
                                        </a>
                                        <button type="submit" class="btn btn-sm btn-danger btn-flat"><span class="material-icons">delete</span></button>
                                    </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody></table>
                    {{ $categories->render()  }}
            </div>
            @else
                <div class="callout callout-warning m-4" role="alert">
                  موردی یافت نشد
                </div>
        @endif

            <!-- /.card-body -->
        </div>
    </div>

@endsection
